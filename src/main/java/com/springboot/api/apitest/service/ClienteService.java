package com.springboot.api.apitest.service;

import com.springboot.api.apitest.bean.ResponseAllCliente;
import com.springboot.api.apitest.model.Cliente;

public interface ClienteService {
	
	ResponseAllCliente getAllClientes();
	ResponseAllCliente getDelClientes(Integer id);
	Cliente getCliente(Integer id);
	void saveCliente(Cliente cliente);
	void deleteCliente(Integer id);
	
}
