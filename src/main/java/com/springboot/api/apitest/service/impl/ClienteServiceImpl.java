package com.springboot.api.apitest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.apitest.bean.ResponseAllCliente;
import com.springboot.api.apitest.dao.impl.ClienteDaoImpl;
import com.springboot.api.apitest.model.Cliente;
import com.springboot.api.apitest.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDaoImpl _clienteDao; 
	
	@Override
	public ResponseAllCliente getAllClientes() {
		ResponseAllCliente response = new ResponseAllCliente();
		List<Cliente> clientes = new ArrayList<Cliente>();
		clientes = _clienteDao.getAllClientes();
		
		if(clientes.size() > 0) {
			response.setCodigo_servicio("0000");
			response.setClientes(clientes);
		}else {
			response.setCodigo_servicio("0100");
			
			
		}
		//return _clienteDao.getAllClientes();
		return response;
	}
	
	@Override
	public ResponseAllCliente getDelClientes(Integer id) {
		ResponseAllCliente response = new ResponseAllCliente();
		Cliente clientes = new Cliente();
		clientes = _clienteDao.getCliente(id);
		
		if(clientes != null) {
			response.setCodigo_servicio("0000 - Cliente no existe.");
			
		}else {
			response.setCodigo_servicio("0100 - Cliente eliminado con éxito.");
			
			
		}

		return response;
	}

	@Override
	public Cliente getCliente(Integer id) {
		
		return _clienteDao.getCliente(id);
	}

	@Override
	public void saveCliente(Cliente cliente) {
		
		_clienteDao.saveCliente(cliente);
	}

	@Override
	public void deleteCliente(Integer id) {
		_clienteDao.deleteCliente(id);
		
	}

}
