package com.springboot.api.apitest.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.springboot.api.apitest.dao.ClienteDao;
import com.springboot.api.apitest.model.Cliente;

@Repository
public class ClienteDaoImpl implements ClienteDao {

	private static List<Cliente> listaclientes = new ArrayList<Cliente>();
	
	 public ClienteDaoImpl() {
		
		 listaclientes.add(new Cliente(1, "Jose Luis", "Huamán", "Villar", "Masculino", "Los olivos", "Casado"));
		 listaclientes.add(new Cliente(2, "Madona", "Madona", "Madona", "Femenino", "EEUU", "Divorciada"));
		 listaclientes.add(new Cliente(3, "Richard", "Paucar", "De la torre", "Masculino", "Los olivos", "Soltero"));
		 listaclientes.add(new Cliente(4, "Bertha", "García", "Esteban", "Femenino", "Pan Norte km. 4", "Casada"));
		 
	}
	
	@Override
	public List<Cliente> getAllClientes() {
		
		return listaclientes;
	}

	@Override
	public Cliente getCliente(Integer id) {
		Cliente cliente = null;
		
		for(int i = 0; i < listaclientes.size(); i++) {
			if(listaclientes.get(i).getId() == id) {
				cliente = listaclientes.get(i);
				return cliente;
			}
		}
		
		return cliente;
	}

	@Override
	public void saveCliente(Cliente cliente) {
		if(cliente.getId() == null || "".equals(cliente.getId())) {
			cliente.setId(listaclientes.size()+1);
			listaclientes.add(cliente);
			
		}else {
			for(int i = 0; i < listaclientes.size(); i++) {
				if(listaclientes.get(i).getId() == cliente.getId()) {
					listaclientes.set(i, cliente);
				}
			}
		}		
		
	}

	@Override
	public void deleteCliente(Integer id) {
		
		for(int i = 0; i <listaclientes.size(); i++ ) {
			Cliente cliente = getCliente(id);
			if(cliente != null) {
				listaclientes.remove(cliente);
			}			
		}		
	}
}
