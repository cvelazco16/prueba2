package com.springboot.api.apitest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.apitest.bean.ResponseAllCliente;
import com.springboot.api.apitest.model.Cliente;
import com.springboot.api.apitest.service.impl.ClienteServiceImpl;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteServiceImpl _clienteService;
	
	@GetMapping(value = "/", produces = "application/json")
	public ResponseAllCliente getAllClientes(){
				
		return _clienteService.getAllClientes();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Cliente getCliente(@PathVariable ("id") Integer id) {
		Cliente cliente = new Cliente();
		cliente = _clienteService.getCliente(id);
		return cliente;
	}
	
	@PostMapping(value = "/", produces = "application/json")
	public ResponseAllCliente saveCliente(@RequestBody Cliente cliente){
		_clienteService.saveCliente(cliente);		
		return _clienteService.getAllClientes();
	}
	
	@DeleteMapping(value = "/{id}", produces = "application/json")
	public ResponseAllCliente deleteCliente(@PathVariable ("id") Integer id){
		_clienteService.deleteCliente(id);		
		return _clienteService.getDelClientes(id);
	}

}
