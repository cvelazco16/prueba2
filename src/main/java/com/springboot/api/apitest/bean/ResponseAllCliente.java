package com.springboot.api.apitest.bean;

import java.util.List;

import com.springboot.api.apitest.model.Cliente;

public class ResponseAllCliente {

	private String codigo_servicio;
	private List<Cliente> clientes;

	public ResponseAllCliente() {

	}

	public ResponseAllCliente(String codigo_servicio, List<Cliente> clientes) {

		this.codigo_servicio = codigo_servicio;
		this.clientes = clientes;
	}

	public String getCodigo_servicio() {
		return codigo_servicio;
	}

	public void setCodigo_servicio(String codigo_servicio) {
		this.codigo_servicio = codigo_servicio;
	}



	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	@Override
	public String toString() {
		return "ResponseAllCliente [codigo_servicio=" + codigo_servicio + ", clientes=" + clientes + "]";
	}






}
